import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class JosefArcs extends PApplet {

/*
 * ::::::::::::::::::
 * RECODE PROJET 2017
 * ::::::::::::::::::
 * ESAC : A2_2017-18
 * Sketch : JosefArcs
 * Source : https://bitbucket.org/cambrai/a2_2017-18
 *
 * Sommaire : Josef M\u00fcller Brockmann
 * REFS : TonHalle : https://www.buamai.com/image/31194-flickr-photo-download-josef-muller-brockmann-2
 *        https://swissincss.com/
 *
 */

public void setup() {
  
  background(0);
  strokeCap(SQUARE);
    float diaArcs = 0;

    for(int i = 0; i<10; i++){
      josefArcs(width/2,height/2, diaArcs);
      diaArcs = diaArcs + 80;
    }
}


public void draw() {}


public void josefArcs(float _x, float _y, float _taille) {
  noFill();
  pushMatrix();
  translate(_x, _y);
  strokeWeight(5);
  for (int i=0; i<360; i+=60) {
    stroke(255, 0, 0);
    arc(0, 0, _taille, _taille, radians(i), radians(i+15));
    stroke(255, 255, 0);
    arc(0, 0, _taille, _taille, radians(i+15), radians(i+30));
     stroke(0, 0, 255);
    arc(0, 0, _taille, _taille, radians(i+30), radians(i+45));
  }
  popMatrix();
}
  public void settings() {  size(800, 800); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--hide-stop", "JosefArcs" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
