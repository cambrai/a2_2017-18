/*
 * ::::::::::::::::::
 * RECODE PROJET 2017
 * ::::::::::::::::::
 * ESAC : A2_2017-18
 * Sketch : Vera_Quad_01
 * Source : https://bitbucket.org/cambrai/a2_2017-18
 *
 * Sommaire : Vera Molnar
 * REFS : http://www.veramolnar.com/
 *
 */

float quadTaille=0;

void setup() {
  size(400, 400);
  background(0);
  noFill();

  translate(width/2, height/2);

   stroke(255);
   for(int i= 0; i<100; i+=5) {
        quadTaille = i;
       quad(-quadTaille, -quadTaille, quadTaille, -quadTaille, quadTaille, quadTaille, -quadTaille, quadTaille);
   }
  }
