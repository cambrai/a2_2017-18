import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class vera_M_01 extends PApplet {

/*
 * ::::::::::::::::::
 * RECODE PROJET 2017
 * ::::::::::::::::::
 * ESAC : A2_2017-18
 * Sketch : Vera_Quad_01
 * Source : https://bitbucket.org/cambrai/a2_2017-18
 *
 * Sommaire : Vera Molnar
 * REFS : http://www.veramolnar.com/
 *
 */

float quadTaille=0;

public void setup() {
  
  background(0);
  noFill();

  translate(width/2, height/2);

   stroke(255);
   for(int i= 0; i<100; i+=5) {
        quadTaille = i;
       quad(-quadTaille, -quadTaille, quadTaille, -quadTaille, quadTaille, quadTaille, -quadTaille, quadTaille);
   }
  }
  public void settings() {  size(400, 400); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--hide-stop", "vera_M_01" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
