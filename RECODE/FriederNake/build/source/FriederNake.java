import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class FriederNake extends PApplet {

/*
 * ::::::::::::::::::
 * RECODE PROJET 2017
 * ::::::::::::::::::
 * ESAC : A2_2017-18
 * Sketch : Frieder Nake Test
 *
 * Sommaire : Test sketch
 * REFS : http://freeartbureau.org/fab_activity/lecture-frieder-nake/
 *
 */
int orange = color(0xffFCAD17);
int fuchsia = color(0xffFC1786);
int noir = color(0);


public void setup() {
  
  noFill();
  strokeWeight(5);
  rectMode(CENTER);
}

public void draw() {
  randomSeed(1);
  background(255);
  stroke(0xffFCAD17);
  // boucle 1 : orange
  for (int i=0; i<100; i++) {
    float x = random(25, 750);
    float y = random(25, 750);

    rect(x, y, 75, 75);
  }

  stroke(0xffFC1786);
  // boucle 2 : fuchsia
  for (int i=0; i<100; i++) {
    float x = random(15, 850);
    float y = random(15, 850);

    rect(x, y, 30, 30);
  }
  stroke(noir);
  // boucle 3 : noir
  for (int i=0; i<5; i++) {
    float x = random(25, 750);
    float y = random(25, 750);

    float s = sin(frameCount*0.025f) * 150;
    rect(x, y, s, s);
  }
}
  public void settings() {  size(900, 900); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#000000", "--hide-stop", "FriederNake" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
