/*
 * ::::::::::::::::::
 * RECODE PROJET 2017
 * ::::::::::::::::::
 * ESAC : A2_2017-18
 * Sketch : Frieder Nake Test
 *
 * Sommaire : Test sketch
 * REFS : http://freeartbureau.org/fab_activity/lecture-frieder-nake/
 *
 */
color orange = color(#FCAD17);
color fuchsia = color(#FC1786);
color noir = color(0);


void setup() {
  size(900, 900);
  noFill();
  strokeWeight(5);
  rectMode(CENTER);
}

void draw() {
  randomSeed(1);
  background(255);
  stroke(#FCAD17);
  // boucle 1 : orange
  for (int i=0; i<100; i++) {
    float x = random(25, 750);
    float y = random(25, 750);

    rect(x, y, 75, 75);
  }

  stroke(#FC1786);
  // boucle 2 : fuchsia
  for (int i=0; i<100; i++) {
    float x = random(15, 850);
    float y = random(15, 850);

    rect(x, y, 30, 30);
  }
  stroke(noir);
  // boucle 3 : noir
  for (int i=0; i<5; i++) {
    float x = random(25, 750);
    float y = random(25, 750);

    float s = sin(frameCount*0.025) * 150;
    rect(x, y, s, s);
  }
}
