# A2_2017-18 #

Hub officiel pour les cours de design num�rique en 2i�me ann�e, men�s par Mark Webster & Keyvane Alinaghi � l'�cole sup�rieur d'art et de communication de Cambrai.

### Contents ###

* Recode (Semestre I)
* ??? (Semestre II)
* Sketches dev
* Sketches �tudiants


### Who do I talk to? ###

* [Mark Webster](https://bitbucket.org/mwebster_/)
* [Keyvane Alinaghi](https://bitbucket.org/keyvane/)
* http://www.esac-cambrai.net/
* Version : 0.1